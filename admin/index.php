<!doctype html>
<html>
    <head>
        <title>Admin panel</title>
        <link href="style.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <div id="header">
            <a href="index.php"><h3>Welcome to Admin panel of amarblog.com</h3></a>
        </div>
        <aside>
            <h2>Manage Content</h2><hr>
            <p><a href="logout.php">Admin Logout</a></p>
            <p><a href="index.php?view=view">Views post</a></p>
            <p><a href="../insert_post.php">Insert New Post</a></p>
        </aside>
        <!--<div class="admin_index">-->
        <?php if(isset($_GET['view'])){ ?>
            <table border="1" width="1000px" align="center">
                <tr><td colspan="8" align="center"><h1>View All Post</h1></td></tr>
                <tr>
                    <th>Serial</th>
                    <th>ID</th>
                    <th>Post Title</th>
                    <th>Post Author</th>
                    <th>Post Date</th>
                    <th>Post Content</th>
                    <th>Post Image</th>
                    <th>Action</th>
                </tr>
                 <?php
                include_once '../includes/connect.php';
                $i=1;
                if(isset($_GET['view'])){
                $q="SELECT * FROM posts order by 1 DESC";
                $run=mysql_query($q);
                while ($row=mysql_fetch_array($run)) {
                	$id=$row['id'];
                	$title=$row['post_title'];
                	$author=$row['author_name'];
                	$content=  substr($row['post_content'], 0,150);
                	$image=$row['post_image'];
                	$date=$row['post_date'];
            ?>
                <tr align="center">
                    <td><?php echo $i++;?></td>
                    <td><?php echo $id; ?></td>
                    <td><?php echo $title; ?></td>
                    <td><?php echo $author; ?></td>
                    <td><?php echo $date; ?></td>
                    <td><?php echo $content; ?></td>
                    <td><img src="../images/<?php echo $image; ?>" width="200px" height="150px"></td>
                    <td>
                        <div style="display: inline;">
                            <a href="edit.php?id=<?php echo $id;?>">Edit</a>
                            <a href="delete.php?id=<?php echo $id;?>">Delete</a>
                        </div>
                    </td>
                </tr>
        <?php }} } ?>
            </table>
           
       <!-- </div>-->
    </body>
</html>


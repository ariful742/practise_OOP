<!doctype html>
<html>
    <head>
        <title>world natures</title>
        <link href="style.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <!-- This is top bar -->
        <div id="top"><p style="color:#fff;">Today is:&nbsp;&nbsp;<?php  echo date("l jS \of F Y h:i:s A");?></p></div>
        <div><?php include('./includes/header.php'); ?></div>
        <div><?php include('./includes/nav.php'); ?></div>
        <div><?php include('./includes/sidebar.php'); ?></div>        
        <div><?php include('./includes/post_body.php'); ?></div>                      
        <div class="foot">This is footer</div>
    </body>
</html>